import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AlphabetSoup {
	public static void main(String args[]) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
			String line = reader.readLine();
			int rows = Integer.parseInt(line.split("x")[0]);
			int columns = Integer.parseInt(line.split("x")[1]);
			char[][] grid = new char[rows][columns];
			int index = 0;
			
			while (index < rows) {
				line = reader.readLine();
				grid[index] = line.replaceAll("\\s", "").toCharArray();
				index++;
			}
			
			while ((line = reader.readLine()) != null) {
				System.out.println(findWord(grid, line.replaceAll("\\s", "")));
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Find given String in a grid.
	 * 
	 * @param grid
	 * @param checkFor
	 * @return
	 */
	private static String findWord(char[][] grid, String checkFor) {
		String retString = checkDiagonal(grid, checkFor);
		if (retString == null) {
			retString = checkRows(grid, checkFor);
		}
		if (retString == null) {
			retString = checkColumns(grid, checkFor);
		}
		return retString;
	}

	/**
	 * Check Diagonal in grid for a given String.
	 * 
	 * @param grid
	 * @param checkFor
	 * @return
	 */
	private static String checkDiagonal(char[][] grid, String checkFor) {
		StringBuilder wordBuilder = new StringBuilder();
		int k = 0;
		int i = 0;
		int numIterations = 0;

		while (numIterations < grid.length) {
			try {
				wordBuilder.append(grid[k][i]);
				if (wordBuilder.toString().contains(checkFor)) {
					String start = "" + (k - checkFor.length() + 1) + ":" + (i - checkFor.length() + 1);
					String end = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				} else if (reverse(wordBuilder.toString()).contains(checkFor)) {
					String end = "" + (k - checkFor.length() + 1) + ":" + (i - checkFor.length() + 1);
					String start = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				}
				i++;
				k++;
			} catch (Exception e) {
				k = 0;
				numIterations++;
				i = numIterations;
				wordBuilder.setLength(0);
			}
		}

		i = 0;
		k = 1;
		numIterations = k;
		while (numIterations < grid[0].length) {
			try {
				wordBuilder.append(grid[k][i]);
				if (wordBuilder.toString().contains(checkFor)) {
					String start = "" + (k - checkFor.length() + 1) + ":" + (i - checkFor.length() + 1);
					String end = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				} else if (reverse(wordBuilder.toString()).contains(checkFor)) {
					String end = "" + (k - checkFor.length() + 1) + ":" + (i - checkFor.length() + 1);
					String start = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				}
				i++;
				k++;
			} catch (ArrayIndexOutOfBoundsException e) {
				numIterations++;
				k = numIterations;
				i = 0;
				wordBuilder.setLength(0);
			}
		}

		i = 0;
		k = 0;
		numIterations = 0;
		while (numIterations < grid[0].length) {
			try {
				wordBuilder.append(grid[k][i]);
				if (wordBuilder.toString().contains(checkFor)) {
					String start = "" + (k - checkFor.length() + 1) + ":" + (i + checkFor.length() - 1);
					String end = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				} else if (reverse(wordBuilder.toString()).contains(checkFor)) {
					String end = "" + (k - checkFor.length() + 1) + ":" + (i + checkFor.length() - 1);
					String start = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				}
				i--;
				k++;
			} catch (ArrayIndexOutOfBoundsException e) {
				numIterations++;
				k = 0;
				i = numIterations;
				wordBuilder.setLength(0);
			}
		}

		i = grid[0].length - 1;
		k = 1;
		numIterations = k;
		while (numIterations < grid[0].length - 1) {
			try {
				wordBuilder.append(grid[k][i]);
				if (wordBuilder.toString().contains(checkFor)) {
					String start = "" + (k - checkFor.length() + 1) + ":" + (i + checkFor.length() - 1);
					String end = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				} else if (reverse(wordBuilder.toString()).contains(checkFor)) {
					String end = "" + (k - checkFor.length() + 1) + ":" + (i + checkFor.length() - 1);
					String start = "" + k + ":" + i;
					return checkFor + " " + start + " " + end;
				}
				i--;
				k++;
			} catch (ArrayIndexOutOfBoundsException e) {
				numIterations++;
				k = numIterations;
				i = grid[0].length - 1;
				wordBuilder.setLength(0);
			}
		}
		return null;
	}

	/**
	 * Check Columns in grid for a given String.
	 * 
	 * @param grid
	 * @param checkFor
	 * @return
	 */
	private static String checkColumns(char[][] grid, String checkFor) {
		StringBuilder columnBuilder = new StringBuilder();
		int index = 0;
		for (int i = 0; i < grid.length; i++) {
			columnBuilder.append(grid[i][index]);
			if (columnBuilder.toString().contains(checkFor)) {
				String start = columnBuilder.toString().indexOf(checkFor) + ":" + index;
				String end = (columnBuilder.toString().indexOf(checkFor) + checkFor.length() - 1) + ":" + index;
				return checkFor + " " + start + " " + end;
			} else if (reverse(columnBuilder.toString()).contains(checkFor)) {
				String start = (columnBuilder.toString().length() - 1
						- reverse(columnBuilder.toString()).indexOf(checkFor)) + ":" + index;
				String end = columnBuilder.toString().indexOf(reverse(checkFor)) + ":" + index;
				return checkFor + " " + start + " " + end;
			}
			if (i == grid.length - 1 && index < grid[0].length - 1) {
				index++;
				i = -1;
				columnBuilder.setLength(0);
			}
		}
		return null;
	}

	/**
	 * Check Rows in grid for a given String.
	 * 
	 * @param grid
	 * @param checkFor
	 * @return
	 */
	private static String checkRows(char[][] grid, String checkFor) {
		for (int i = 0; i < grid.length; i++) {
			String row = new String(grid[i]);
			if (row.contains(checkFor)) {
				String start = i + ":" + row.indexOf(checkFor);
				String end = i + ":" + (row.indexOf(checkFor) + checkFor.length() - 1);
				return checkFor + " " + start + " " + end;
			} else if (reverse(row).contains(checkFor)) {
				int j = reverse(row).indexOf(checkFor);
				String start = i + ":" + (row.length() - 1 - j);
				String end = i + ":" + row.indexOf(reverse(checkFor));
				return checkFor + " " + start + " " + end;
			}
		}
		return null;
	}

	/**
	 * Reverses a given String.
	 * 
	 * @param s
	 * @return
	 */
	private static String reverse(String s) {
		return new StringBuffer(s).reverse().toString();
	}
}